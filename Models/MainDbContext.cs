﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace KanbanBoard.Models
{
    public class MainDbContext:DbContext
    {
        public DbSet<Task> Tasks { get; set; }

        public MainDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=KanbanDB;Trusted_Connection=True;");
        }
    }

    public static class DbInitializer
    {
        public static void Initialize(MainDbContext context)
        {
            context.Database.EnsureCreated();
            if (context.Tasks.Any())
            {
                return;   // DB has been seeded
            }
            Task t1 = new Task() { Title = "task1", Description = "desc1" };
            Task t2 = new Task() { Title = "task2", Description = "desc2", Status = (int) TaskStatus.InProgress };
            Task t3 = new Task() { Title = "task3", Description = "desc3", Status = (int) TaskStatus.Complited };
            Task t4 = new Task() { Title = "task4", Description = "desc4", Status = (int) TaskStatus.Complited };
            Task t5 = new Task() { Title = "task5", Description = "desc5" };
            Task t6 = new Task() { Title = "task6", Description = "desc6" };
            Task t7 = new Task() { Title = "task7", Description = "desc7", Status = (int) TaskStatus.Complited };
            context.Tasks.AddRange(t1,t2,t3,t4,t5,t6,t7);
            context.SaveChanges();
        }
    }
}
