﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanbanBoard.Models
{
    public enum TaskStatus
    {
        Active = 0,
        InProgress = 1,
        Complited = 2
    }
    public class Task
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; } = "";
        public int Status { get; set; } = 0;
    }
}
